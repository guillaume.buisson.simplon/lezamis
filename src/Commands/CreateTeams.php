<?php


namespace App\Commands;


use App\Entity\Team;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Faker;

class CreateTeams extends Command
{
    protected static $defaultName = 'app:create:teams';

    private $em;

    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->em = $entityManager;
    }

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $faker = Faker\Factory::create();
        $output->writeln([
            'Teams Creator',
            '============',
            '',
        ]);

        for ($i = 0;$i <= 20;$i++)
        {
            $arrayUsers = [];
            $team = new Team();
            $team->setName($faker->name);
            for ($p = 0;$p <= 10;$p++)
            {
                $user = new User();
                $user->setUsername($faker->userName);
                $user->setRoles([]);
                $user->setPassword($faker->password);
                $user->setApiToken(uniqid());
                $this->em->persist($user);
                $team->addMember($user);
            }
            $this->em->persist($team);
        }

        $this->em->flush();
        $output->writeln('Finished');

        return 0;
    }
}