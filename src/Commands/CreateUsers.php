<?php


namespace App\Commands;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUsers extends Command
{
    protected static $defaultName = 'app:create:user';

    private $em;

    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->em = $entityManager;
    }

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $users = [
            [
                "username" => "SoCloud",
                "password" => "admin",
                "apiToken" => uniqid()
            ],
            [
                "username" => "SoTwinky",
                "password" => "admin",
                "apiToken" => uniqid()
            ]
        ];

        foreach ($users as $user) {
            $checkUser = $this->em->getRepository(User::class)->findByUsername($user["username"]);
            if(count($checkUser) === 0) {
                $newUser = new User();
                $newUser->setUsername($user['username']);
                $newUser->setApiToken($user["apiToken"]);
                $newUser->setRoles([]);
                $newUser->setPassword($user['password']);
                $this->em->persist($newUser);
            }
        }
        $this->em->flush();
        $output->writeln('Finished');

        return 0;
    }
}