<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getOneUsersById($id): Response
    {
        $user = $this->em->getRepository(User::class)->find($id);
        $array = [
            "id" => $user->getId(),
            "username" => $user->getUsername(),
            "apiToken" => $user->getApiToken(),
        ];
        return $this->json($array);
    }
}
