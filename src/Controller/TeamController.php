<?php


namespace App\Controller;


use App\Entity\Team;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class TeamController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getAllMembersByOneTeam($id): Response
    {
        $team = $this->em->getRepository(Team::class)->find($id);
        $members = $team->getMembers();
        dd($members);
//        $array = [
//            "id" => $user->getId(),
//            "username" => $user->getUsername(),
//            "apiToken" => $user->getApiToken(),
//        ];
//        return $this->json($array);
    }
}